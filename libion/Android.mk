LOCAL_PATH:= $(call my-dir)

##################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := ion.c
ifeq ($(strip $(TARGET_BOARD_PLATFORM)),rk312x)
LOCAL_SRC_FILES := rk_ion.c
LOCAL_C_INCLUDES += hardware/rockchip/common/include/
endif #rk312x

LOCAL_MODULE := libion
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := liblog
include $(BUILD_SHARED_LIBRARY)


##################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := ion.c ion_test.c
ifeq ($(strip $(TARGET_BOARD_PLATFORM)),rk312x)
LOCAL_SRC_FILES := rk_ion.c rk_ion_test.c
LOCAL_C_INCLUDES += hardware/rockchip/common/include/
endif #rk312x

LOCAL_MODULE := iontest
LOCAL_MODULE_TAGS := optional tests
LOCAL_SHARED_LIBRARIES := liblog
include $(BUILD_EXECUTABLE)
